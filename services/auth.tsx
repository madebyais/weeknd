import {cookieKey} from "common/constant";
import jwt from "jsonwebtoken"

export default class AuthService {
  private readonly cookies: any
  private readonly cookie: string
  private readonly token: any

  constructor(cookies: any) {
    this.cookies = cookies
    this.cookie = this.cookies[cookieKey]
    this.token = jwt.verify(this.cookie, process.env.SECRET_HASH!)
  }

  public getToken() {
    return this.token.id && this.token.fullName && this.token.subscriptionType ? this.token : undefined
  }

  public getUserId() {
    return this.token.id || undefined
  }

  public getFullName() {
    return this.token.fullName || undefined
  }

  public getSubscriptionType() {
    return this.token.subscriptionType || undefined
  }
}