import AccountRepository from "repositories/account";
import SubscriptionRepository from "repositories/subscription";
import PageRepository from "repositories/page";
import {IPage, SUBSCRIPTION_TYPES} from "repositories/schemas";
import SubscriptionService from "./subscription";

export default class PageService {
  private accountRepository: AccountRepository
  private pageRepository: PageRepository
  private subscriptionRepository: SubscriptionRepository

  private subscriptionService: SubscriptionService

  constructor() {
    this.accountRepository = new AccountRepository()
    this.pageRepository = new PageRepository()
    this.subscriptionRepository = new SubscriptionRepository()

    this.subscriptionService = new SubscriptionService()
  }

  public async create(page: IPage, subscriptionType: SUBSCRIPTION_TYPES) {
    let result: any = {}

    result = await this.pageRepository.findByAccountId(page.accountId)
    if (result.errors) {
      console.dir({ method: 'PageService.create', when: 'this.pageRepository.findByAccountId', error: result.errors[0].message })
      result.errors = [{message: 'Terjadi kesalahan. Coba kembali nanti.'}]
      return result
    }

    if (result.data.page.length >= this.subscriptionService.getMaxTotalCreatedPage(subscriptionType)) {
      console.dir({ method: 'PageService.create', when: 'this.subscriptionService.getMaxTotalCreatedPage', error: 'reached max limit to create a new page' })
      result.errors = [{message: 'Akunmu sudah mencapai batas maksimal pembuatan Page. Yuk, upgrade akunmu untuk mendapatkan limit yang lebih banyak lagi!'}]
      return result
    }

    result = await this.pageRepository.findBySlug(page.slug)
    if (result.errors) {
      console.dir({ method: 'PageService.create', when: 'this.pageRepository.findBySlug', error: result.errors[0].message })
      result.errors = [{message: 'Terjadi kesalahan. Coba kembali nanti.'}]
      return result
    }

    if (result.data && result.data.page && result.data.page.length > 0) {
      result.errors = [{message: 'Slug sudah terdaftar.'}]
      return result
    }

    result = await this.pageRepository.create(page)
    if (result.errors) {
      console.dir({ method: 'PageService.create', when: 'this.pageRepository.create', error: result.errors[0].message })
      result.errors = [{message: 'Gagal membuat Page baru. Coba kembali nanti.'}]
      return result
    }

    result.data = result.data.insert_page_one
    return result
  }

  public async find(id: string, accountId: string) {
    let result: any = await this.pageRepository.findByIdAndAccountId(id, accountId)
    if (result.errors) {
      console.dir({ method: 'PageService.find', when: 'this.pageRepository.findByIdAndAccountId', error: result.errors[0].message })
      result.errors = [{message: 'Terjadi kesalahan. Coba kembali nanti.'}]
      return result
    }

    result.data = result.data.page
    return result
  }

  public async findBySlug(slug: string) {
    let result: any = await this.pageRepository.findBySlug(slug)
    if (result.errors) {
      console.dir({ method: 'PageService.findBySlug', when: 'this.pageRepository.findBySlug', error: result.errors[0].message })
      result.errors = [{message: 'Terjadi kesalahan. Coba kembali nanti.'}]
      return result
    }

    result.data = result.data.page.length ? result.data.page[0] : {}
    return result
  }

  public async findAll(accountId: string) {
    let result: any = await this.pageRepository.findByAccountId(accountId)
    if (result.errors) {
      console.dir({ method: 'PageService.findAll', when: 'this.pageRepository.findByAccountId', error: result.errors[0].message })
      result.errors = [{message: 'Terjadi kesalahan. Coba kembali nanti.'}]
      return result
    }

    result.data = result.data.page
    return result
  }

  public async updateElementDraft(id: string, accountId: string, elementDraft: any) {
    let keys: string = `$elementDraft: jsonb`
    let set: string = `elementDraft: $elementDraft`
    let result: any = await this.pageRepository.update(keys, set, {id, accountId, elementDraft})
    if (result.errors) {
      console.dir({ method: 'PageService.updateElementDraft', when: 'this.pageRepository.update', error: result.errors[0].message })
      result.errors = [{message: 'Terjadi kesalahan. Coba kembali nanti.'}]
      return result
    }

    result.data = result.data.page
    return result
  }

  public async updateElementPublished(id: string, accountId: string, elementPublished: any) {
    let keys: string = `$elementPublished: jsonb`
    let set: string = `elementPublished: $elementPublished`
    let result: any = await this.pageRepository.update(keys, set, {id, accountId, elementPublished})
    if (result.errors) {
      console.dir({ method: 'PageService.updateElementPublished', when: 'this.pageRepository.update', error: result.errors[0].message })
      result.errors = [{message: 'Terjadi kesalahan. Coba kembali nanti.'}]
      return result
    }

    result.data = result.data.page
    return result
  }

  public async updatePageDetail(id: string, accountId: string, page: any) {
    let result: any = await this.pageRepository.findBySlug(page.slug)
    if (result.errors) {
      console.dir({ method: 'PageService.updatePageDetail', when: 'this.pageRepository.findBySlug', error: result.errors[0].message })
      result.errors = [{message: 'Terjadi kesalahan. Coba kembali nanti.'}]
      return result
    }

    if (result.data && result.data.page && result.data.page.length > 0 && result.data.page[0].id != id) {
      result.errors = [{message: 'Slug sudah terdaftar.'}]
      return result
    }

    let keys: string = `$title: String, $slug: String, $description: String, $metadata: jsonb`
    let set: string = `title: $title, slug: $slug, description: $description, metadata: $metadata`
    delete page.type

    result = await this.pageRepository.update(keys, set, {id, accountId, ...page})
    if (result.errors) {
      console.dir({ method: 'PageService.updatePageDetail', when: 'this.pageRepository.update', error: result.errors[0].message })
      result.errors = [{message: 'Terjadi kesalahan. Coba kembali nanti.'}]
      return result
    }

    result.data = result.data.page
    return result
  }
}