import {SUBSCRIPTION_TYPES} from "repositories/schemas";

export default class SubscriptionService {
  constructor() {}

  public getMaxTotalCreatedPage(type: SUBSCRIPTION_TYPES) {
    let max: number = 0

    switch (type) {
      case SUBSCRIPTION_TYPES.FREE_TRIAL:
        max = 1
        break
      case SUBSCRIPTION_TYPES.STARTER:
        max = 5
        break
      case SUBSCRIPTION_TYPES.PRO:
        max = 20
    }

    return max
  }
}