import {cookieKey} from "common/constant";
import {Modal} from "antd";

export const isUnauthorized = (cookies: any) => {
  return !cookies[cookieKey] ? true : false
}

export const redirectUnauthorizedUsingModal = () => {
  Modal.error({ title: 'Error', content: 'Mohon maaf, sesi anda telah kedaluwarsa.', onOk: () => { window.location.href = '/' } })
}

export const redirectUnauthorizedServerSideProps = {
  redirect: {
    destination: '/',
    permanent: false,
  }
}