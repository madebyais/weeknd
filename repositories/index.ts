import Graphql from "../util/graphql";

const dbExec = async (query: any, variables: any) => {
  return await new Graphql(process.env, query, variables).execute()
}

export default dbExec