interface IDefaultSchema {
  id?: string
  createdAt?: any
  updatedAt?: any
  deletedAt?: any
}

export interface IAccount extends IDefaultSchema {
  username: string
  email: string
  fullName: string
  secretKey?: string
  phoneNo?: string
  picUrl?: string
  metadata?: any
}

export interface ISubscription extends IDefaultSchema {
  accountId: string
  type: SUBSCRIPTION_TYPES
  metadata?: any
  expiredAt?: any
}

export interface ISubscriptionHistory extends IDefaultSchema {
  subscriptionId: string
  type: SUBSCRIPTION_TYPES
  currency: string
  price: number
  expiredAt?: any
  metadata?: any
}

export enum SUBSCRIPTION_TYPES {
  FREE_TRIAL = 'FREE_TRIAL',
  STARTER = 'STARTER',
  PRO = 'PRO',
  ENTERPRISE = 'ENTERPRISE'
}

export interface IPage extends IDefaultSchema {
  accountId: string
  category: string
  title: string
  slug: string
  description?: string
  elementDraft?: any
  elementPublished?: any
  metadata?: IPageMetadata
}

interface IPageMetadata {
  googleAnalyticsTrackingCode?: string
}

export interface ITemplateMarketplace extends IDefaultSchema {
  accountId: string
  title: string
  slug: string
  description?: any
  element?: any
}

export interface ITemplateOwned extends IDefaultSchema {
  templateMarketplaceId: string
  accountId: string
  status?: TEMPLATE_OWNED_STATUS
}

export enum TEMPLATE_OWNED_STATUS {
  PENDING = 'PENDING',
  PAID = 'PAID',
  CANCELLED = 'CANCELLED'
}