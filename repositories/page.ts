import dbExec from "./index";
import {IPage} from "./schemas";
import {v1 as uuidv1} from "uuid"

export default class PageRepository {
  private readonly fields: string

  constructor() {
    this.fields = `id
      accountId
      category
      title
      slug
      description
      elementDraft
      elementPublished
      metadata
      createdAt
      updatedAt`
  }

  public async findBySlug(slug: string) {
    const query = `query pageFindBySlug($slug: String) {
      page(where: {slug: {_eq: $slug}}) {
        ${this.fields}
      }
    }`

    return await dbExec(query, {slug})
  }

  public async findByAccountId(accountId: string) {
    const query = `query pageFindByAccountId($accountId: String) {
      page(where: {accountId: {_eq: $accountId}}) {
        ${this.fields}
      }
    }`

    return await dbExec(query, {accountId})
  }

  public async findByIdAndAccountId(id: string, accountId: string) {
    const query = `query pageFindByIdAndAccountId($id: String, $accountId: String) {
      page(where: {
        id: {_eq: $id},
        accountId: {_eq: $accountId}
      }) {
        ${this.fields}
      }
    }`

    return await dbExec(query, {id, accountId})
  }

  public async create(page: IPage) {
    page.id = uuidv1()
    page.updatedAt = new Date()
    const query = `mutation pageCreate(
      $id: String,
      $accountId: String,
      $category: String,
      $title: String,
      $slug: String,
      $description: String,
      $elementDraft: jsonb = [],
      $elementPublished: jsonb = [],
      $metadata: jsonb = {},
      $updatedAt: timestamp = ""
    ) {
      insert_page_one(object: {
        id: $id,
        accountId: $accountId,
        category: $category,
        title: $title,
        slug: $slug,
        description: $description,
        elementDraft: $elementDraft,
        elementPublished: $elementPublished,
        metadata: $metadata,
        updatedAt: $updatedAt
      }) {
        ${this.fields}
      }
    }`

    return await dbExec(query, page)
  }

  public async update(keys: string, set: string, values: any) {
    const query = `mutation pageCreate(
      $id: String,
      $accountId: String,
      $updatedAt: timestamp = "",
      ${keys}
    ) {
      update_page(
        where: {
          id: {_eq: $id},
          accountId: {_eq: $accountId}
        },
        _set: {
          updatedAt: $updatedAt,
          ${set}
        }
      ) {
        returning {
          ${this.fields}
        }
      }
    }`

    let opts: any = {
      ...values,
      updatedAt: new Date()
    }
    return await dbExec(query, opts)
  }
}