export default [
  {
    name: 'Gray',
    types: [
      { name: '400', color: 'gray-400' },
      { name: '500', color: 'gray-500' },
      { name: '600', color: 'gray-600' },
      { name: '700', color: 'gray-700' }
    ]
  },
  {
    name: 'Red',
    types: [
      { name: '400', color: 'red-400' },
      { name: '500', color: 'red-500' },
      { name: '600', color: 'red-600' },
      { name: '700', color: 'red-700' }
    ]
  },
  {
    name: 'Yellow',
    types: [
      { name: '400', color: 'yellow-400' },
      { name: '500', color: 'yellow-500' },
      { name: '600', color: 'yellow-600' },
      { name: '700', color: 'yellow-700' }
    ]
  },
  {
    name: 'Green',
    types: [
      { name: '400', color: 'green-400' },
      { name: '500', color: 'green-500' },
      { name: '600', color: 'green-600' },
      { name: '700', color: 'green-700' }
    ]
  },
  {
    name: 'Blue',
    types: [
      { name: '400', color: 'blue-400' },
      { name: '500', color: 'blue-500' },
      { name: '600', color: 'blue-600' },
      { name: '700', color: 'blue-700' }
    ]
  },
  {
    name: 'Indigo',
    types: [
      { name: '400', color: 'indigo-400' },
      { name: '500', color: 'indigo-500' },
      { name: '600', color: 'indigo-600' },
      { name: '700', color: 'indigo-700' }
    ]
  },
  {
    name: 'Purple',
    types: [
      { name: '400', color: 'purple-400' },
      { name: '500', color: 'purple-500' },
      { name: '600', color: 'purple-600' },
      { name: '700', color: 'purple-700' }
    ]
  },
  {
    name: 'Pink',
    types: [
      { name: '400', color: 'pink-400' },
      { name: '500', color: 'pink-500' },
      { name: '600', color: 'pink-600' },
      { name: '700', color: 'pink-700' }
    ]
  }
]
