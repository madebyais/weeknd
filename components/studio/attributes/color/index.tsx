import {FC, useState} from "react";
import ColorSchema from "./colors";

interface IColorAttributesProps {
  onChange: Function
}

const ColorAttributes: FC<IColorAttributesProps> = ({ onChange, children }) => {
  const [selected, setSelected] = useState<string>('')
  const onClick = (color: any, primaryColor: string) => {
    setSelected(primaryColor)
    onChange(primaryColor)
  }

  return (
    <>
      {ColorSchema.map((color: any, i: number) => (
        <div key={i} className={`bg-${color.name.toLowerCase()}-100 border border-${color.name.toLowerCase()}-500 grid grid-cols-5 space-x-2 pl-2 pr-3 py-1 uppercase rounded-full mb-1 flex items-center`}>
          <div className={`font-bold text-${color.name.toLowerCase()}-500`} style={{fontSize: 8}}>{color.name}</div>
          {color.types.map((c: any, ci: number) => (
            <div key={ci}
                 className={`bg-${c.color} cursor-pointer hover:bg-${color.name.toLowerCase()}-300 text-center text-white w-8 h-8 rounded-full flex justify-center items-center ${selected == c.color ? 'border border-4 border-'+color.name.toLowerCase()+'-900':''}`}
                 onClick={e => onClick(color, c.color)}
                 style={{fontSize: 9}}>
              {c.name}
            </div>
          ))}
        </div>
      ))}
    </>
  )
}

export default ColorAttributes