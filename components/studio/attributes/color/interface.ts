export interface IColorAttributeProps {
  name: string
  types: Array<IColorClassProps>
}

export interface IColorClassProps {
  name: string
  color: string
}