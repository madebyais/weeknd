export default class ColorUtil {
  private readonly color: string

  constructor(color: string) {
    this.color = color
  }

  public getBaseColor(): string {
    return this.color && this.color.split('-') ? this.color.split('-')[0] : ''
  }

  public getTextColor(): string {
    return `text-${this.color}`
  }

  public getBackgroundColor(): string {
    return `bg-${this.color}`
  }

  public getBorderColor(): string {
    return `border border-${this.color}`
  }
}