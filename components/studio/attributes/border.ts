export const BorderSchema: any = {
  type: 'string',
  title: 'Border',
  enum: [ 'none', 'border' ],
  enumNames: [ 'None', 'Border' ]
}

export const BorderRadiusSchema: any = {
  type: 'string',
  title: 'Border Radius',
  enum: [ 'none', 'rounded', 'rounded-lg', 'rounded-full' ],
  enumNames: [ 'None', 'Normal', 'Large', 'Full' ]
}
