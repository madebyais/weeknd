export interface IGoogleAnalyticsEventProps {
  gaEventName: string
  gaEventVariables: string
}

export const GoogleAnalyticsEventSchema: any = {
  gaEventName: {
    type: 'string',
    title: 'GA Event Name'
  },
  gaEventVariables: {
    type: 'string',
    title: 'GA Event Variables'
  }
}

export const gaSendEvent = ({gaEventName, gaEventVariables}: IGoogleAnalyticsEventProps) => {
  if (gaEventName && gaEventVariables) {
    eval(`gtag('event', '${gaEventName}', ${gaEventVariables})`)
  }
}