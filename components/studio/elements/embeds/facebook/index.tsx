import {FC} from "react";
import {PositionSchema} from "components/studio/attributes/position";
import {getMarginClassName, IMarginSchemaProps, MarginSchema} from "components/studio/attributes/margin";
import {IElementProps} from "components/studio/elements/interfaces";
import UiWidget from "components/studio/attributes/ui-widget";

interface IFacebookProps extends IMarginSchemaProps {
  url: string
  width: number
  height: number
  position: string
}

const Facebook: FC<IElementProps> = ({ metadata, style }) => {
  const props: IFacebookProps = metadata
  const {url, width, height, position = ''} = props
  const margin: string = getMarginClassName(props)
  const className: string = `${position} ${margin}`

  if (!url || url == '') return <>Click edit icon to edit the facebook element attributes.</>
  return (
    <div className={className}>
      <iframe
        src={`https://www.facebook.com/plugins/post.php?href=${encodeURIComponent(url)}&show_text=true&appId=126492130885165`}
        width={width ? width.toString() : "400"} height={height ? height.toString() : "340"} style={{border:'none', overflow:'hidden'}} scrolling="no" frameBorder="0"
        allowFullScreen={true}
        allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
    </div>
  )
}

export default Facebook

export const FacebookSchema: any = {
  form: {
    type: 'object',
    properties: {
      url: {
        type: 'string',
        title: 'Facebook Post URL'
      },
      // width: {
      //   type: 'number',
      //   title: 'Width'
      // },
      // height: {
      //   type: 'number',
      //   title: 'Height'
      // },
      position: PositionSchema,
      ...MarginSchema
    }
  },
  ui: {
    position: UiWidget.SELECT
  }
}