import {gaSendEvent, GoogleAnalyticsEventSchema, IGoogleAnalyticsEventProps} from "components/studio/attributes/ga-event";
import {IElementProps} from "components/studio/elements/interfaces";
import ColorUtil from "components/studio/attributes/color/util";
import UiWidget from "components/studio/attributes/ui-widget";

interface IBlockHero1Props extends IGoogleAnalyticsEventProps {
  title: string
  description: string
  imageUrl: string
  linkText: string
  linkUrl: string
}

export default function BlockHero1({ metadata, style }: IElementProps) {
  const { title, description, imageUrl, linkText, linkUrl, gaEventName, gaEventVariables }: IBlockHero1Props = metadata
  const color: ColorUtil = new ColorUtil(style.primaryColor)

  return (
    <section>
      <div className="container flex flex-col items-center px-5 py-8 mx-auto">
        <div className="flex flex-col w-full mb-12 text-left lg:text-center">
          <div
            className="inline-flex items-center justify-center flex-shrink-0 w-20 h-20 mx-auto mb-5 text-black bg-blueGray-100 rounded-full">
            {imageUrl ? (
              <img src={imageUrl} width={80} height={80} />
            ):(
              <svg xmlns="http://www.w3.org/2000/svg" className="w-10 h-10 icon icon-tabler icon-tabler-aperture"
                   width="24" height="24" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" fill="none"
                   strokeLinecap="round" strokeLinejoin="round">
                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                <circle cx="12" cy="12" r="9"></circle>
                <line x1="3.6" y1="15" x2="14.15" y2="15"></line>
                <line x1="3.6" y1="15" x2="14.15" y2="15" transform="rotate(72 12 12)"></line>
                <line x1="3.6" y1="15" x2="14.15" y2="15" transform="rotate(144 12 12)"></line>
                <line x1="3.6" y1="15" x2="14.15" y2="15" transform="rotate(216 12 12)"></line>
                <line x1="3.6" y1="15" x2="14.15" y2="15" transform="rotate(288 12 12)"></line>
              </svg>
            )}
          </div>
          <h1 className={`mx-auto mb-8 text-2xl font-semibold leading-none tracking-tighter ${color ? color.getTextColor() : `text-black`} lg:w-1/2 lg:text-3xl title-font`}>
            {title || `A small headline to switch your visitors into users.`}
          </h1>
          <p className="mx-auto text-base font-medium leading-relaxed text-blueGray-700 lg:w-1/2">
            {description || `You're about to launch soon and must be 100% focused on your product. Don't loose precious days designing, coding the landing page and testing.`}
          </p>
          <a onClick={e => gaSendEvent({ gaEventName, gaEventVariables })} href={linkUrl || `#`} className={`mx-auto mt-8 text-sm font-semibold ${color ? color.getTextColor() : `text-blue-600`} hover:text-black" title="read more`}>
            {linkText || `Read more about the offer »`}
          </a>
        </div>
      </div>
    </section>
  )
}

export const BlockHero1Schema: any = {
  form: {
    type: 'object',
    properties: {
      title: {
        type: 'string',
        title: 'Title'
      },
      description: {
        type: 'string',
        title: 'Description'
      },
      imageUrl: {
        type: 'string',
        title: 'Image URL (80x80)'
      },
      linkText: {
        type: 'string',
        title: 'Link Text'
      },
      linkUrl: {
        type: 'string',
        title: 'Link URL'
      },
      ...GoogleAnalyticsEventSchema
    }
  },
  ui: {
    description: UiWidget.TEXTAREA
  }
}