import {IElementProps} from "components/studio/elements/interfaces";
import ColorUtil from "components/studio/attributes/color/util";
import UiWidget from "components/studio/attributes/ui-widget";

interface IBlockTestimonial1Props {
  imageUrl: string
  text: string
  customerName: string
}

export default function BlockTestimonial1 ({ metadata, style }: IElementProps) {
  const { imageUrl, text, customerName }: IBlockTestimonial1Props = metadata
  const color: ColorUtil = new ColorUtil(style.primaryColor)

  return (
    <section className="mx-auto">
      <div className="container px-5 mx-auto lg:px-24 lg:py-20">
        <div className="flex flex-col w-full mb-12 text-left lg:text-center">
          <img
            alt="testimonial"
            className="inline-block object-cover object-center w-20 h-20 mx-auto mb-8 rounded-full"
            src={imageUrl || `https://dummyimage.com/300x300/F3F4F7/8693ac`} />
          <p className="mx-auto text-base font-medium leading-relaxed text-blueGray-700 lg:w-1/2">
            {text || `You're about to launch soon and must be 100% focused on your product. Don't loose precious days designing, coding the landing page and testing .`}
          </p>
          <h2 className={`mt-4 text-xs font-semibold tracking-widest ${color ? color.getTextColor() : 'text-black'} uppercase title-font`}>
            {customerName || 'John Doe'}
          </h2>
        </div>
      </div>
    </section>
  )
}

export const BlockTestimonial1Schema: any = {
  form: {
    type: 'object',
    properties: {
      imageUrl: {
        type: 'string',
        title: 'Image URL (300x300)'
      },
      text: {
        type: 'string',
        title: 'Text'
      },
      customerName: {
        type: 'string',
        title: 'Customer Name'
      }
    }
  },
  ui: {
    text: UiWidget.TEXTAREA
  }
}