import {gaSendEvent, GoogleAnalyticsEventSchema, IGoogleAnalyticsEventProps} from "../../../attributes/ga-event";
import UiWidget from "components/studio/attributes/ui-widget";
import {IElementProps} from "components/studio/elements/interfaces";
import ColorUtil from "components/studio/attributes/color/util";

interface IBlockCTA1Props extends IGoogleAnalyticsEventProps {
  title: string
  description: string
  buttonText: string
  buttonUrl: string
}

export default function BlockCTA1({ metadata, style }: IElementProps) {
  const { title, description, buttonText, buttonUrl, gaEventName, gaEventVariables}: IBlockCTA1Props = metadata
  const color: ColorUtil = new ColorUtil(style.primaryColor)

  return (
    <section className={`py-20 bg-white`}>
      <div className={`px-4 mx-auto text-center max-w-7xl sm:px-6 lg:px-8`}>
        <h2 className={`text-3xl font-extrabold tracking-tight text-${color.getBaseColor() || 'indigo'}-600 sm:text-4xl md:text-5xl xl:text-6xl`}>
          {title || `The New Standard for Design`}
        </h2>
        <p className={`max-w-md mx-auto mt-3 text-base text-gray-500 sm:text-lg md:mt-5 md:text-xl md:max-w-3xl`}>
          {description || 'Use our award-winning tools to help you maximize your profits. We\'ve uncovered the correct recipe for converting visitors into customers.'}
        </p>
        <div className={`flex justify-center mt-8 space-x-3`}>
          <a href={buttonUrl || `#`} onClick={e => gaSendEvent({gaEventName, gaEventVariables})} className={`inline-flex items-center justify-center px-5 py-3 text-base font-medium text-white bg-${color.getBaseColor() || 'indigo'}-600 border border-transparent rounded-md shadow hover:bg-${color.getBaseColor() || 'indigo'}-700 hover:text-${color.getBaseColor() || 'indigo'}-200`}>{buttonText || `Sign Up Today`}</a>
        </div>
      </div>
    </section>
  )
}

export const BlockCTA1Schema: any = {
  form: {
    type: 'object',
    properties: {
      title: {
        type: 'string',
        title: 'Title Text'
      },
      description: {
        type: 'string',
        title: 'Description Text'
      },
      buttonText: {
        type: 'string',
        title: 'Button Text'
      },
      buttonUrl: {
        type: 'string',
        title: 'Button Target URL'
      },
      ...GoogleAnalyticsEventSchema
    }
  },
  ui: {
    type: UiWidget.SELECT,
    description: UiWidget.TEXTAREA
  }
}