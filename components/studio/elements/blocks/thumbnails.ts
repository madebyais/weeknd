import {ElementTypes} from "../types";

const BlockThumbnails: any = [
  { icon: 'fa-flash', title: 'Click-to-Action 1', type: ElementTypes.BLOCK_CTA_1 },
  { icon: 'fa-flash', title: 'Click-to-Action 2', type: ElementTypes.BLOCK_CTA_2 },

  { icon: 'fa-newspaper-o', title: 'Hero 1', type: ElementTypes.BLOCK_HERO_1 },
  { icon: 'fa-newspaper-o', title: 'Hero 2', type: ElementTypes.BLOCK_HERO_2 },
  { icon: 'fa-newspaper-o', title: 'Hero 3', type: ElementTypes.BLOCK_HERO_3 },
  { icon: 'fa-newspaper-o', title: 'Hero 4', type: ElementTypes.BLOCK_HERO_4 },

  { icon: 'fa-money', title: 'Pricing 1', type: ElementTypes.BLOCK_PRICING_1 },

  { icon: 'fa-id-card', title: 'Testimonial 1', type: ElementTypes.BLOCK_TESTIMONIAL_1 },
]

export default BlockThumbnails