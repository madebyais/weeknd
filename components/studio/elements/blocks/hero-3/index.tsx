import {IElementProps} from "components/studio/elements/interfaces";
import ColorUtil from "components/studio/attributes/color/util";
import UiWidget from "components/studio/attributes/ui-widget";

interface IBlockHero3Props {
  tagline: string
  title: string
  description: string
}

export default function BlockHero3({ metadata, style }: IElementProps) {
  const { tagline, title, description }: IBlockHero3Props = metadata
  const color: ColorUtil = new ColorUtil(style.primaryColor)

  return (
    <section className="text-blueGray-700 ">
      <div className="container flex flex-col items-center px-5 py-8 mx-auto">
        <div className="flex flex-col w-full mb-12 text-left lg:text-center">
          <h2 className="mb-8 text-xs font-semibold tracking-widest text-black uppercase title-font">
            {tagline || 'a great tagline right here'}
          </h2>
          <h1 className={`mx-auto mb-12 text-2xl font-semibold leading-none tracking-tighter ${color ? color.getTextColor() : 'text-indigo-600'} lg:w-1/2 sm:text-6xl title-font`}>
            {title || 'A Long headline to switch your visitors into users.'}
          </h1>
          <p className="mx-auto text-base font-medium leading-relaxed text-blueGray-700 lg:w-1/2">
            {description || 'You\'re about to launch soon and must be 100% focused on your product. Don\'t loose precious days designing, coding the landing page and testing.'}
          </p>
        </div>
      </div>
    </section>
  )
}

export const BlockHero3Schema: any = {
  form: {
    type: 'object',
    properties: {
      tagline: {
        type: 'string',
        title: 'Tagline'
      },
      title: {
        type: 'string',
        title: 'Title'
      },
      description: {
        type: 'string',
        title: 'Description'
      }
    }
  },
  ui: {
    description: UiWidget.TEXTAREA
  }
}