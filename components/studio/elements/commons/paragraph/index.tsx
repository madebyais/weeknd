import {FC} from "react";
import {TextAlignSchema} from "components/studio/attributes/text";
import {FontSizeSchema, FontWeightSchema} from "components/studio/attributes/font";
import {getMarginClassName, IMarginSchemaProps, MarginSchema} from "components/studio/attributes/margin";
import {IElementProps} from "components/studio/elements/interfaces";
import ColorUtil from "components/studio/attributes/color/util";
import UiWidget from "components/studio/attributes/ui-widget";

interface IParagraphProps extends IMarginSchemaProps {
  text: string
  textAlign?: string
  fontSize?: string
  fontWeight?: string
}

const Paragraph: FC<IElementProps> = ({ metadata, style }) => {
  const props: IParagraphProps = metadata
  const color: ColorUtil = new ColorUtil(style.primaryColor)

  const margin: string = getMarginClassName(props)
  const textColor: string = color.getTextColor()
  const { text, textAlign, fontSize, fontWeight } = props

  const className: string = `${textAlign} ${fontSize} ${fontWeight} ${textColor} ${margin}`

  if (!text || text == '') return <>Click here to edit the paragraph element attributes.</>
  return (
    <p className={className}>{text}</p>
  )
}

export default Paragraph

export const ParagraphSchema: any = {
  form: {
    type: 'object',
    properties: {
      text: {
        type: 'string',
        title: 'Text'
      },
      textAlign: TextAlignSchema,
      fontSize: FontSizeSchema,
      fontWeight: FontWeightSchema,
      ...MarginSchema
    }
  },
  ui: {
    text: UiWidget.TEXTAREA,
    textAlign: UiWidget.SELECT,
    fontSize: UiWidget.SELECT,
    fontWeight: UiWidget.SELECT
  }
}