import {FC, useEffect, useState} from "react";
import {getPaddingClassName, IPaddingSchemaProps, PaddingSchema} from "components/studio/attributes/padding";
import {getMarginClassName, IMarginSchemaProps, MarginSchema} from "components/studio/attributes/margin";
import {TextAlignSchema} from "components/studio/attributes/text";
import {FontSizeSchema, FontWeightSchema} from "components/studio/attributes/font";
import {IElementProps} from "components/studio/elements/interfaces";
import ColorUtil from "components/studio/attributes/color/util";
import UiWidget from "components/studio/attributes/ui-widget";

interface IQuoteProps extends IPaddingSchemaProps, IMarginSchemaProps {
  text: string
  textAlign?: string
  fontSize?: string
  fontWeight?: string
  backgroundColor?: string
}

const Quote: FC<IElementProps> = ({ metadata, style }) => {
  const props: IQuoteProps = metadata
  const color: ColorUtil = new ColorUtil(style.primaryColor)

  const [className, setClassName] = useState<string>('')
  const [containerClassName, setContainerClassName] = useState<string>('')

  useEffect(() => {
    const textColor: string = color.getTextColor()
    const { textAlign, fontSize, fontWeight } = props
    const padding: string = getPaddingClassName(props)
    const margin: string = getMarginClassName(props)

    setClassName(`${textAlign} ${fontSize} ${fontWeight} ${textColor}`)
    setContainerClassName(`bg-${color.getBaseColor()}-200 ${padding} ${margin}`)
  }, [style])

  if (!props.text || props.text == '') return <>Click here to edit the quote element attributes.</>
  return (
    <div className={containerClassName}>
      <div className={`${props.textAlign} mb-1`}><i className={`fa fa-quote-right ${color.getTextColor()}`} /></div>
      <div className={`${className} italic`}>{props.text}</div>
    </div>
  )
}

export default Quote

export const QuoteSchema: any = {
  form: {
    type: 'object',
    properties: {
      text: {
        type: 'string',
        title: 'Text'
      },
      textAlign: TextAlignSchema,
      fontSize: FontSizeSchema,
      fontWeight: FontWeightSchema,
      ...PaddingSchema,
      ...MarginSchema
    }
  },
  ui: {
    textAlign: UiWidget.SELECT,
    fontSize: UiWidget.SELECT,
    fontWeight: UiWidget.SELECT
  }
}