import {FC} from "react";
import {IElementProps} from "components/studio/elements/interfaces";

interface IImageProps {
  url: string
  alt: string
  width: number
  height: number
}

const Image: FC<IElementProps> = ({ metadata, style }) => {
  const props: IImageProps = metadata
  const {url, alt, width, height} = props

  let customImageStyle: any = {}
  if (width && width > 0) customImageStyle.maxWidth = width
  if (height && height > 0) customImageStyle.maxHeight = height

  if (!url || url == '') return <div className={`text-center`}>Hover here and click edit icon to edit the image element attributes.</div>
  return (
    <img className={`w-full mx-auto`} src={url || `https://cdn.devdojo.com/images/november2020/hero-image.jpeg`} alt={alt} style={customImageStyle} />
  )
}

export default Image

export const ImageSchema: any = {
  form: {
    type: 'object',
    properties: {
      url: {
        type: 'string',
        title: 'Image URL'
      },
      alt: {
        type: 'string',
        title: 'Alternative Information'
      },
      width: {
        type: 'number',
        title: 'Width'
      },
      height: {
        type: 'number',
        title: 'Height'
      }
    }
  }
}