import {FC} from "react";
import {FontSizeSchema, FontWeightSchema} from "components/studio/attributes/font";
import {getPaddingClassName, IPaddingSchemaProps, PaddingSchema} from "components/studio/attributes/padding";
import {getMarginClassName, IMarginSchemaProps, MarginSchema} from "components/studio/attributes/margin";
import {BorderRadiusSchema} from "components/studio/attributes/border";
import {PositionSchema} from "components/studio/attributes/position";
import {IElementProps} from "components/studio/elements/interfaces";
import ColorUtil from "components/studio/attributes/color/util";
import UiWidget from "components/studio/attributes/ui-widget";

interface ILinkProps extends IPaddingSchemaProps, IMarginSchemaProps {
  text: string
  url: string
  isOpenInNewTab?: boolean
  position?: boolean
  borderRadius?: string
  fontSize?: string
  fontWeight?: string
}

const Link: FC<IElementProps> = ({ metadata, style }) => {
  const props: ILinkProps = metadata
  const color: ColorUtil = new ColorUtil(style.primaryColor)

  const padding: string = getPaddingClassName(props)
  const margin: string = getMarginClassName(props)
  const baseColor: string = color.getBaseColor()
  const bgColor: string = color.getBackgroundColor()
  const textColor: string = color.getTextColor()
  const { text, url, isOpenInNewTab, position, fontSize, fontWeight, borderRadius } = props

  const className: string = `inline-block ${bgColor} ${fontSize} ${fontWeight} ${borderRadius} hover:bg-${baseColor}-800 ${margin} ${padding} ${baseColor != 'white' ? `text-white hover:text-white` : textColor}`

  if (!text || text == '') return <>Click here to edit the link element attributes.</>
  return (
    <div className={`${position}`}>
      <a href={url}
         target={isOpenInNewTab ? '_blank' : '_parent'}
         className={className}
         rel="noreferrer">
        {text}
      </a>
    </div>
  )
}

export default Link

export const LinkSchema: any = {
  form: {
    type: 'object',
    properties: {
      text: {
        type: 'string',
        title: 'Text'
      },
      url: {
        type: 'string',
        title: 'Target URL'
      },
      isOpenInNewTab: {
        type: 'boolean',
        title: 'Open in New Tab?'
      },
      position: PositionSchema,
      borderRadius: BorderRadiusSchema,
      fontSize: FontSizeSchema,
      fontWeight: FontWeightSchema,
      ...PaddingSchema,
      ...MarginSchema
    }
  },
  ui: {
    isOpenInNewTab: UiWidget.RADIO,
    textAlign: UiWidget.SELECT,
    fontSize: UiWidget.SELECT,
    fontWeight: UiWidget.SELECT
  }
}