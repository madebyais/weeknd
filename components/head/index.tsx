import Head from "next/head";
import {FC} from "react";

interface IPageHeadProps {
  title: string
}

const PageHead: FC<IPageHeadProps> = ({ title }) => {
  return (
    <Head>
      <title>{title}</title>
    </Head>
  )
}

export default PageHead