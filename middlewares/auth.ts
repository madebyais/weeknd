import {NextApiHandler, NextApiRequest, NextApiResponse} from "next";
import {cookieKey} from "../common/constant";

const middlewareAuth = (handler: NextApiHandler) => {
  return async (req: NextApiRequest, res: NextApiResponse) => {
    if (!req.cookies[cookieKey] && !req.query.query_type)
      return res.status(401).json({ success: false, error: 'Unauthorized' })

    return handler(req, res)
  }
}

export default middlewareAuth