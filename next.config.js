module.exports = {
  reactStrictMode: true,

  webpack: function (config) {
    config.module.rules.push({
      test: /\.ya?ml$/,
      use: 'js-yaml-loader'
    })

    return config
  },

  images: {
    domains: ['placeimg.com']
  },

  env: {
    BASE_URL: process.env.BASE_URL,
    SECRET_HASH: process.env.SECRET_HASH,

    GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
    GOOGLE_REDIRECT_URI: process.env.GOOGLE_REDIRECT_URI,
    GOOGLE_OAUTH_SCOPE: process.env.GOOGLE_OAUTH_SCOPE,

    GOOGLE_ANALYTICS_ENABLE: process.env.GOOGLE_ANALYTICS_ENABLE || false,
    GOOGLE_ANALYTICS_TRACKING_ID: process.env.GOOGLE_ANALYTICS_TRACKING_ID,

    GRAPHQL_URL: process.env.GRAPHQL_URL,
    GRAPHQL_SECRET: process.env.GRAPHQL_SECRET
  }
}
