import {NextApiRequest, NextApiResponse} from "next";
import PageService from "services/page";
import AuthService from "services/auth";
import middlewareAuth from "middlewares/auth";

const api = async (req: NextApiRequest, res: NextApiResponse) => {
  const pageService: PageService = new PageService()
  const authService: AuthService = new AuthService(req.cookies)

  try {
    let result: any = {}
    switch (req.method?.toLowerCase()) {
      case 'post':
        req.body.accountId = authService.getUserId()
        result = await pageService.create(req.body, authService.getSubscriptionType())
        break
      default:
        result = await pageService.findAll(authService.getUserId())
    }

    const {errors, data} = result
    if (errors && errors.length) {
      return res.status(500).json({ success: false, error: errors[0].message })
    }

    res.status(200).json({ success: true, data })
  } catch (e) {
    console.dir({ api: '/api/v1/page', error: e })
    res.status(500).json({ success: false, error: e })
  }
}

export default middlewareAuth(api)