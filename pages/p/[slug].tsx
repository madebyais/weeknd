import {IElementProps} from "components/studio/elements/interfaces";
import {NextPageContext} from "next";
import Fetch from "util/fetch";
import {ElementGenerator} from "components/studio/elements";
import Head from "next/head";
import {IPage} from "repositories/schemas";
import {FC} from "react";

interface IPageProps {
  baseUrl?: string
  page: IPage
}

const Page: FC<IPageProps> = ({ baseUrl= '', page }) => {
  return (
    <div>
      <Head>
        <title>{page.title}</title>
        <meta property="og:title" content={page.title} />
        <meta property="og:description" content={page.description} />
        <meta property="og:url" content={`${baseUrl}/p/${page.slug}`} />
        <meta property="og:type" content="website" />
        {/*<meta property="og:image" content="https://ahrefs.com/blog/wp-content/uploads/2019/12/fb-how-to-become-an-seo-expert.png" />*/}

        <meta name="twitter:card" content="product" />
        <meta name="twitter:title" content={page.title} />
        <meta name="twitter:description" content={page.description} />
        <meta name="twitter:url" content={`${baseUrl}/p/${page.slug}`} />
        {/*<meta name="twitter:image" content={page.image} />*/}

        {page.metadata?.googleAnalyticsTrackingCode &&
          <>
            <script async src={`https://www.googletagmanager.com/gtag/js?id=${page.metadata?.googleAnalyticsTrackingCode}`} />
            <script
              dangerouslySetInnerHTML={{
                __html: `
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', '${page.metadata?.googleAnalyticsTrackingCode}', {
                  page_path: window.location.pathname,
                });
              `,
              }}></script>
          </>
        }
      </Head>
      <main>
        {page.elementPublished.map((elem: IElementProps, i: number) => (
          <ElementGenerator key={i} {...elem} />
        ))}
        <div className={`text-center text-xs my-10`}>Designed with <a href={`https://weeknd.id`} className={`font-semibold`}>WEEKND</a></div>
      </main>
    </div>
  )
}

export default Page

export async function getServerSideProps(ctx: NextPageContext) {
  const { query } = ctx

  const result: any = await Fetch('GET', `${process.env.BASE_URL}/api/v1/page/${query.slug}?query_type=slug`)
  if (!result.success || result.success && !result.data.elementPublished) {
    return { notFound: true }
  }

  return {
    props: {
      baseUrl: process.env.BASE_URL || '',
      page: result.data
    }
  }
}