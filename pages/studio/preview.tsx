import {useEffect, useState} from "react";
import {IElementProps} from "components/studio/elements/interfaces";
import {ElementGenerator} from "components/studio/elements";
import Head from "next/head";
import {NextPageContext} from "next";
import {parseCookies} from "nookies";
import {isUnauthorized, redirectUnauthorizedUsingModal} from "util/page-server-side";

export default function StudioPreviewPage({ isUnauthorized }: any) {
  const [elements, setElements] = useState<Array<IElementProps>>([])

  useEffect(() => {
    if (isUnauthorized) {
      redirectUnauthorizedUsingModal()
      return
    }

    if (process.browser) { // @ts-ignore
      setElements(JSON.parse(localStorage.getItem('draft-elements')))
    }
  }, [])

  return (
    <div>
      <Head>
        <title>Preview</title>
      </Head>
      <main>
        <div className={`container mx-auto`}>
          {elements.map((elem, i) => (
            <ElementGenerator key={i} {...elem} />
          ))}
          <div className={`text-center text-xs mt-5`}>Designed with <a href={`https://weeknd.id`} className={`font-semibold`}>WEEKND</a></div>
        </div>
      </main>
    </div>
  )
}

export async function getServerSideProps(ctx: NextPageContext) {
  const cookies: any = parseCookies(ctx)

  return {
    props: {
      isUnauthorized: isUnauthorized(cookies)
    }
  }
}