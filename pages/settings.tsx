import Layout from "./_layout";
import {Tabs} from "antd";
import {NextPageContext} from "next";
import {parseCookies} from "nookies";
import {
  isUnauthorized,
  redirectUnauthorizedServerSideProps
} from "util/page-server-side";
import AuthService from "services/auth";
import SubscriptionService from "services/subscription";

export default function Settings({ currentSubscriptionType }: any) {
  return (
    <Layout title={`Settings`}>
      <div className={`container mx-auto py-5 grid grid-cols-1`}>
        <Tabs type={`card`} defaultActiveKey={`profile`}>
          <Tabs.TabPane tab={`Langganan`} key={`subscription`}>
            <Subscription currentSubscriptionType={currentSubscriptionType} />
          </Tabs.TabPane>
        </Tabs>
      </div>
    </Layout>
  )
}

export async function getServerSideProps(ctx: NextPageContext) {
  const cookies: any = parseCookies(ctx)
  if (isUnauthorized(cookies)) return redirectUnauthorizedServerSideProps

  const authService = new AuthService(cookies)

  return {
    props: {
      currentSubscriptionType: authService.getSubscriptionType()
    }
  }
}

const Profile = () => (<div>Profile</div>)

const Subscription = ({ currentSubscriptionType }: any) => {
  const subscriptionService = new SubscriptionService()

  return (
    <div className={`px-5 md:px-0`}>
      <div className={`mb-20`}>
        <div className={`text-xs uppercase`}>Tipe langgananmu saat ini</div>
        <div className={`uppercase font-bold text-2xl text-indigo-600`}>{currentSubscriptionType.replace('_', ' ')}</div>
      </div>

      <div className={`grid grid-cols-1 md:grid-cols-3 gap-5`}>
        <div className={`flex flex-col justify-between border hover:border-indigo-600 p-5 rounded`}>
          <div>
            <div className={`font-semibold text-lg mb-1`}>STARTER</div>
            <div className={`font-black text-4xl`}>Rp 129,000 <span className={`text-xs`}>/ bulan</span></div>
            <div className={`line-through text-lg text-red-400`}>Rp 189,000</div>

            <div className={`my-10 space-y-2`}>
              <SubscriptionFeature text={<>Kamu akan mendapatkan <strong>5 Pages</strong></>} />
              <SubscriptionFeature text={<>Akses hanya ke STARTER block element</>} />
              <SubscriptionFeature text={<>Integrasi dengan <strong>Google Analytics</strong></>} />
              <SubscriptionFeature text={<>Tidak perlu pusing merawat server</>} />
              <SubscriptionFeature text={<><strong>3% dari nominal</strong> yang kamu bayarkan akan kami sedekahkan untuk membantu sesama</>} />
            </div>
          </div>

          <div>
            <a href={`#`} className={`block py-3 border border-indigo-600 rounded uppercase font-semibold text-indigo-600 text-center`}>Subscribe</a>
          </div>
        </div>
        <div className={`flex flex-col justify-between bg-indigo-600 text-white border border-indigo-600 p-5 rounded`}>
          <div>
            <div className={`font-semibold text-lg mb-1`}>PRO</div>
            <div className={`font-black text-4xl`}>Rp 269,000 <span className={`text-xs`}>/ bulan</span></div>
            <div className={`line-through text-lg text-red-400`}>Rp 329,000</div>

            <div className={`my-10 space-y-2`}>
              <SubscriptionFeature text={<>Kamu akan mendapatkan <strong>20 Pages</strong></>} />
              <SubscriptionFeature text={<>Akses ke semua block element</>} />
              <SubscriptionFeature text={<>Akses fitur <strong className={`uppercase`}>Trending Keyword</strong></>} />
              <SubscriptionFeature text={<>Integrasi dengan <strong>Google Analytics</strong></>} />
              <SubscriptionFeature text={<>Tidak perlu pusing merawat server</>} />
              <SubscriptionFeature text={<>Akses ke fitur terbaru yang akan kami release dalam waktu dekat</>} />
              <SubscriptionFeature text={<><strong>5% dari nominal</strong> yang kamu bayarkan akan kami sedekahkan untuk membantu sesama</>} />
            </div>
          </div>

          <div>
            <a href={`#`} className={`block py-3 border rounded uppercase font-semibold text-white text-center`}>Subscribe</a>
          </div>
        </div>
        {/*<div className={`flex flex-col justify-between border hover:border-indigo-600 p-5 rounded`}>*/}
        {/*  <div>*/}
        {/*    <div className={`font-semibold text-lg mb-1`}>ENTERPRISE</div>*/}
        {/*    <div className={`font-black text-4xl`}>Kontak Kami</div>*/}

        {/*    <div className={`my-10 space-y-2`}>*/}
        {/*      <SubscriptionFeature text={<>Kamu yang tentukan total Pages yang kamu inginkan</>} />*/}
        {/*      <SubscriptionFeature text={<>Semua Fitur <strong>PRO</strong></>} />*/}
        {/*      <SubscriptionFeature text={<><strong>5% dari nominal</strong> yang kamu bayarkan akan kami sedekahkan untuk membantu sesama</>} />*/}
        {/*    </div>*/}
        {/*  </div>*/}

        {/*  <div>*/}
        {/*    <a href={`#`} className={`block py-3 border border-indigo-600 rounded uppercase font-semibold text-indigo-600 text-center`}>Subscribe</a>*/}
        {/*  </div>*/}
        {/*</div>*/}
      </div>
    </div>
  )
}

const SubscriptionFeature = ({ text }: any) => (
  <div className={`flex`}>
    <div className={`pr-3`}><i className={`fa fa-check-circle-o`} /></div>
    <div>{text}</div>
  </div>
)